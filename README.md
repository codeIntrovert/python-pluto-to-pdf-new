# PLUTO to PDF Converter

This Python module generates PDF procedures from properply formatted PLUTO scripts.

The PDF procedures contain:
- a cover page
- a flowchart page
- one or more pages of procedure steps

## Getting Started

```
git clone https://gitlab.com/librecube/prototypes/python-pluto-to-pdf
cd python-pluto-to-pdf
python -m venv venv
. venv/bin/activate
pip install -e .
pip install git+https://gitlab.com/librecube/prototypes/python-pluto-parser.git
```

## Example

Check the PLUTO examples in the examples folder. Convert them like this:

```
cd examples
pluto_to_pdf simple.pluto simple.pdf
```

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/xxx/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/xxx

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube documentation](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
