from colorama import Fore, Back, Style
class colorText:


    def magenta(msg:str):
        print(Fore.MAGENTA + Style.BRIGHT + msg + Style.RESET_ALL)

    def warning(msg:str):
        print(Fore.YELLOW + Style.BRIGHT+ msg +Style.RESET_ALL)

    def success(msg:str):
        print(Fore.GREEN + Style.BRIGHT + msg + Style.RESET_ALL)

    def cyan(msg:str):
        print(Fore.CYAN + msg + Style.RESET_ALL)

    def green(msg:str):
        print(Fore.GREEN + msg + Style.RESET_ALL)

    def error(msg:str):
        print(Fore.RED + Style.BRIGHT + msg + Style.RESET_ALL)