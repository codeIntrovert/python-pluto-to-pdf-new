from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, PageBreak, Preformatted
from reportlab.lib import colors
from .colorText import colorText
import os

def create_cover_page(pluto_file_path: str):
    colorText.cyan("Generating cover page...")
    elements = []

    # Get the path of the current module
    module_path = os.path.dirname(__file__)

    # Construct the path to the image file
    image_path = os.path.join(module_path, "assets", "logo_librecube.png")

    if os.path.exists(image_path):
        # If the image file exists, add it to the elements
        image = Image(image_path, width=200, height=36)
        elements.append(image)
    else:
        # If the image file does not exist, print a warning message
        print("Warning: Image file '{}' not found.".format(image_path))

    cover_style = ParagraphStyle(
        name='Cover', 
        fontSize=8, 
        leading=14, 
        alignment=1,
        textColor='#36454F',
        fontName='Helvetica-Bold',
        spaceBefore=10)

    code_style = ParagraphStyle(
        name='Code', 
        fontSize=8, 
        leading=14, 
        alignment=0,
        textColor='#36454F',
        fontName='Courier',
        backgroundColor=colors.lightgrey,
        spaceBefore=10)

    code = ""
    '''
    example parsed code:
    procedure
    main
        log "XYX";
        log "ABC";
    end main

    end procedure
    '''
    with open(pluto_file_path) as f:
        code = f.read()
    
    elements.append(Paragraph("This is an Auto Generated PDF", cover_style))
    elements.append(Paragraph(f"PLUTO File: {pluto_file_path}", cover_style))
    # Use Preformatted for code to maintain line breaks and whitespace
    elements.append(Preformatted(code, code_style))

    return elements
